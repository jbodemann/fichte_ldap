extern crate base64;
extern crate clap;
extern crate exitcode;
extern crate passwords;

use askama::Template;
use clap::{App, Arg};
use passwords::PasswordGenerator;
use pickledb::{PickleDb, PickleDbDumpPolicy, SerializationMethod};
use std::fs::File;
use std::io::{BufRead, BufReader, Error, Write};
use std::process;
use std::str;

// Filename for last_uid store.
const DB_NAME: &str = "last_uid.db";

#[derive(Template)]
#[template(path = "ldap-user.txt")]
struct LdifUserTemplate<'a> {
    login: &'a str,
    first_name: &'a str,
    last_name: &'a str,
    display_name: &'a str,
    uid: &'a str,
    password: &'a String,
}

#[allow(dead_code)]
#[derive(Template)]
#[template(path = "ldap-group.txt")]
struct LdifGroupTemplate<'a> {
    login: &'a str,
    first_name: &'a str,
    display_name: &'a str,
    last_name: &'a str,
    uid: &'a str,
    password: &'a String,
}

// Store for manipulated records from csv file.
struct Student {
    login: String,
    user_id: String,
    password: String,
    first_name: String,
    last_name: String,
    display_name: String,
}

fn main() -> Result<(), pickledb::error::Error> {
    let matches = App::new("fichte-ldap")
        .version("0.2")
        .author("Jörn Bodemann <jb@opv.de>")
        .about("Generate .ldif file to import users into ldap server from user records in a csv file.")
        .arg(Arg::with_name("input")
            .short("i")
            .long("input")
            .value_name("FILE")
            .help("File with student information in csv format. Format: ID,first_names(space separated),last_names(space separated). The ID needs to be unique for all students. Defaults to students.csv")
            .takes_value(true))
        .arg(Arg::with_name("output")
            .short("o")
            .long("output")
            .value_name("FILE")
            .help("Filename for the generated result. Defaults to ldap_user.ldif")
            .takes_value(true))
        .arg(Arg::with_name("prefix")
            .short("p")
            .long("prefix")
            .value_name("PREFIX")
            .help("The string prefixing the ids. IDs are used as login names. Since numbers are not allowed as login names in systemd based Linux systems we prefix them. Defaults to 'f'")
            .takes_value(true))
        .arg(Arg::with_name("lastuid") // Todo nach telefonat mit Frog korrigieren
            .short("l")
            .long("lastuid")
            .value_name("NUMBER")
            .help("Last uid that is used on the ldap server. Generation starts with the next number. Defaults to the last uid written or 10000 on first start.")
            .takes_value(true))
        .arg(Arg::with_name("no-header")
            .short("n")
            .long("no-header")
            .help("Use this flag to indicate the csv file has no header. Defaults assumption is a header exists.")
            .takes_value(false))
        .arg(Arg::with_name("student-passwords")
            .short("s")
            .long("student-passwords")
            .help("Use this flag to create a csv file with names and passwords.")
            .takes_value(false))
        .arg(
            Arg::with_name("delimiter")
                .short("d")
                .long("delimiter")
                .required(false)
                .value_name("ASCII")
                .takes_value(true)
                .help("The record delimiter of the csv file. Defaults to ';'"))
        .get_matches();

    if matches.is_present("student-passwords") {
        create_passwords_csv_file();
        process::exit(0);
    }
    let input = matches.value_of("input").unwrap_or("students.csv");
    let output = matches.value_of("output").unwrap_or("ldap_user.ldif");
    let prefix = matches.value_of("prefix").unwrap_or("f");
    let delimiter_string = matches.value_of("delimiter").unwrap_or(";");
    let mut last_uid = matches
        .value_of("lastuid")
        .unwrap_or("10000")
        .parse::<u32>()
        .expect("lastuid is not a number!");
    let mut header = true;
    if matches.is_present("no-header") {
        header = false;
    }
    let delimiter = *delimiter_string
        .as_bytes()
        .iter()
        .nth(0)
        .expect("delimiter should have at least one char!");

    // Create or open db to store last_uid
    let mut db = match PickleDb::load(
        DB_NAME,
        PickleDbDumpPolicy::AutoDump,
        SerializationMethod::Json,
    ) {
        Ok(db) => db,
        Err(_) => {
            let mut db = PickleDb::new(
                DB_NAME,
                PickleDbDumpPolicy::AutoDump,
                SerializationMethod::Json,
            );
            db.set("lastuid", &last_uid)?;
            db
        }
    };
    last_uid = match db.get::<u32>("lastuid") {
        None => {
            // We could open the database file but did not find lastuid.
            // This can only happen if the file got manipulated.
            db.set("lastuid", &last_uid)?;
            last_uid
        }
        Some(id) => id,
    };

    let result =
        generate_students_ldif_config_file(input, output, last_uid + 1, prefix, delimiter, header);
    {
        match result {
            Ok(records) => {
                println!(
                    "Done. {} student records written into file: {}",
                    records, output
                );
                match db.set("lastuid", &(&last_uid + records)) {
                    Ok(_) => {}
                    Err(err) => {
                        println!("ERROR updating {}, please check result for last uid and use lastuid parameter next run. Please report the error to jb@opv.de", err)
                    }
                }
                std::process::exit(exitcode::OK);
            }
            Err(err) => {
                eprintln!("Error: {}", err);
                std::process::exit(exitcode::DATAERR);
            }
        }
    }
}

// Read student records from csv file. Prepare data and write to result file.
fn generate_students_ldif_config_file(
    input_filename: &str,
    output_filename: &str,
    last_uid: u32,
    prefix: &str,
    delimiter: u8,
    header: bool,
) -> Result<u32, Error> {
    let file =
        File::open(input_filename).expect(format!("File {} not found", input_filename).as_ref());

    let mut reader = csv::ReaderBuilder::new()
        .delimiter(delimiter)
        .has_headers(header)
        .flexible(true)
        .from_reader(file);

    let pg = PasswordGenerator::new()
        .length(12)
        .numbers(true)
        .lowercase_letters(true)
        .uppercase_letters(true)
        .symbols(false)
        .spaces(false)
        .exclude_similar_characters(true)
        .strict(true);

    let mut students = vec![];
    let mut index = 0;
    while let Some(result) = reader.records().next() {
        // for record in reader.records() {
        let record = result.unwrap();
        match record.len() {
            1 => {
                println!("ERROR: Record has only one field, but should have 3. Possibly wrong delimiter. Delimiter used:{} {:?}", delimiter as char, record);
                continue;
            }
            0 | 2 => {
                println!("ERROR: Missing fields in record: {:?}", record);
                continue;
            }
            3 => (),
            _ => {
                println!("ERROR: Too many fields in record: {:?}", record);
                continue;
            }
        }
        let display_name = format!("{} {}", &record[1], &record[2]);
        let student = Student {
            login: format!("{}{}", prefix, &record[0]),
            user_id: (index + last_uid).to_string(),
            password: pg.generate_one().unwrap(),
            first_name: base64::encode(&record[1]),
            last_name: base64::encode(&record[2]),
            display_name: base64::encode(display_name),
        };
        index += 1;
        students.push(student);
    }

    let handle = match File::create(output_filename) {
        Ok(val) => val,
        Err(err) => {
            println!("Could not create output file: {}", output_filename);
            return Err(err);
        }
    };

    // Write user part of ldif file.
    for student in &students {
        let ldif = LdifUserTemplate {
            login: student.login.as_ref(),
            first_name: student.first_name.as_ref(),
            display_name: student.display_name.as_ref(),
            last_name: student.last_name.as_ref(),
            uid: student.user_id.as_ref(),
            password: &student.password,
        };
        writeln!(&handle, "{}", ldif.render().unwrap())?;
        writeln!(&handle, "")?;
    }
    // Write group part of ldif file.
    for student in &students {
        let ldif = LdifGroupTemplate {
            login: student.login.as_ref(),
            first_name: student.first_name.as_ref(),
            display_name: student.display_name.as_ref(),
            last_name: student.last_name.as_ref(),
            uid: student.user_id.as_ref(),
            password: &student.password,
        };
        writeln!(&handle, "{}", ldif.render().unwrap())?;
        writeln!(&handle, "")?;
    }
    Ok(index)
}

fn create_passwords_csv_file() {
    let handle = match File::create("students-password-info.csv") {
        Ok(val) => val,
        Err(err) => {
            println!(
                "Could not create output file: {}. ERROR: {}",
                "students-password-info.csv", err
            );
            process::exit(1);
        }
    };

    let filename = "ldap_user.ldif";
    // Open the file in read-only mode (ignoring errors).
    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);

    // Read the file line by line using the lines() iterator from std::io::BufRead.
    let mut name: Vec<u8> = vec![];
    let mut login = "".to_string();
    let mut pwd: String;
    for line in reader.lines() {
        let line = line.unwrap(); // Ignore errors.
        if line.contains("displayName") {
            name = base64::decode(line.replace("displayName:: ", "")).unwrap();
        }
        if line.contains("uid:") {
            login = line.replace("uid: ", "");
        }
        if line.contains("userPassword") {
            pwd = line.replace("userPassword: ", "");
            let s = match str::from_utf8(&name) {
                Ok(v) => v,
                Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
            };
            writeln!(&handle, "{};{};{}", login, s, pwd)
                .expect("ERROR writing to file students-password-info.csv");
            // println!("{:?};{}", s, pwd);
        }
        // Show the line and its number.
    }
}
